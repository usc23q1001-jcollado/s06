from abc import ABC, abstractmethod

class Person(ABC):
    def __init__(self):
        self.first_name = "Justin"
        self.last_name = "Collado"

    def get_full_name(self):
        print(f"{self.first_name} {self.last_name}")
    
    
    def add_request(self):
        print("Request added")

    
    def check_request(self):
        pass

    
    def add_user(self):
        pass

class Employee(Person):
    def __init__(self, first_name,last_name,email,department):
            super().__init__()
            self.first_name = first_name
            self.last_name = last_name
            self.email = email
            self.department = department

    def set_fname(self,first_name):
        self.first_name =first_name
    def set_lname(self,last_name):
        self.last_name = last_name
    def set_email(self,email):
        self.email = email
    def set_department(self,first_name):
        self.department = department
    def get_fname(self):
        print(f'First Name: {self.first_name}')
    def get_lname(self):
        print(f'Last Name: {self.last_name}')
    def get_email(self):
        print(f'Email: {self.email}')
    def get_department(self):
        print(f'Dept: {self.department}')

    def add_user(self):
        print("User added")

    def login(self):
        print(f'{self.email} has logged in')

    def logout(self):
        print(f'{self.email} has logged out')

    def check_request(self):
        print("Approved")

class TeamLead(Person):
    def __init__(self, first_name,last_name,email,department):
            super().__init__()
            self.first_name = first_name
            self.last_name = last_name
            self.email = email
            self.department = department

    def set_fname(self,first_name):
        self.first_name =first_name
    def set_lname(self,last_name):
        self.last_name = last_name
    def set_email(self,email):
        self.email = email
    def set_department(self,first_name):
        self.department = department
    def get_fname(self):
        print(f'First Name: {self.first_name}')
    def get_lname(self):
        print(f'Last Name: {self.last_name}')
    def get_email(self):
        print(f'Email: {self.email}')
    def get_department(self):
        print(f'Dept: {self.department}')

    def add_user(self):
        print("User added")

    def login(self):
        print(f'{self.email} has logged in')

    def logout(self):
        print(f'{self.email} has logged out')

    def check_request(self):
        print("Approved")

    # def addMember(obj):
           

class Admin(Person):
    def __init__(self, first_name,last_name,email,department):
            super().__init__()
            self.first_name = first_name
            self.last_name = last_name
            self.email = email
            self.department = department

    def set_fname(self,first_name):
        self.first_name =first_name
    def set_lname(self,last_name):
        self.last_name = last_name
    def set_email(self,email):
        self.email = email
    def set_department(self,first_name):
        self.department = department
    def get_fname(self):
        print(f'First Name: {self.first_name}')
    def get_lname(self):
        print(f'Last Name: {self.last_name}')
    def get_email(self):
        print(f'Email: {self.email}')
    def get_department(self):
        print(f'Dept: {self.department}')

    def add_user(self):
        print("User added")

    def login(self):
        print(f'{self.email} has logged in')

    def logout(self):
        print(f'{self.email} has logged out')

    def check_request(self):
        print("Approved")

class Request():
    def __init__(self,name,requester,date_requested):
        self.name = name
        self.requester = requester
        self.date_requested = date_requested

    def set_status(self,status):
        self.status = status

    def update_request():
        print("request updated")
    def close_request():
        print("requst closed")
    def close_cancel():
        print("request cancel") 



e1 = Employee("John", "Doe","ujizz@email","Marketing")
e2 = Employee("J","Rizz","jrizz@email", "Sales")
e3 = Employee("Ba","Saw","bsaw@email", "Sales")
e4 = Employee("Arnold","Njga","aNjga@email", "Marketing")
a1 = Admin("Adminu","Acid", "AA@email.com", "Marketing")
t1 = TeamLead("Papa", "Gsauce", "gsauce@email.com","Marketing")
r1 = Request("New hire",t1,"Oct 2, 2023")
r2 = Request("Laptop Repair",e1,"July 2 2009")
e1.get_full_name()
a1.get_full_name()
t1.get_full_name()
e1.login()
e1.add_request()
e1.logout()
# t1.addMember(e1)
